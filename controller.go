/**
* Created by Visual Studio Code.
* User: tuxer
* Created At: 2018-04-12 14:51:46
**/

package net

//ControllerInterface ...
type ControllerInterface interface {
	ReceiveRequest(req *Request, writer *HTTPWriter)

	setClient(client *Client)
	setServer(server *Server)
}

//Controller ...
type Controller struct {
	ControllerInterface
	server *Server
	client *Client
}

//GetClient ...
func (c *Controller) GetClient() *Client {
	return c.client
}

//GetServer ...
func (c *Controller) GetServer() *Server {
	return c.server
}

//setClient ...
func (c *Controller) setClient(client *Client) {
	c.client = client
}

//setClient ...
func (c *Controller) setServer(server *Server) {
	c.server = server
}

//OnConnected ...
func (c *Controller) OnConnected() {
}

//OnDisconnected ...
func (c *Controller) OnDisconnected() {
}

//ReceiveRequest ...
func (c *Controller) ReceiveRequest(req *Request, writer *HTTPWriter) {

}
