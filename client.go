/**
* Created by Visual Studio Code.
* User: tuxer
* Created At: 2018-01-26 15:00:51
**/

package net

import (
	"crypto/tls"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"
)

//Client ...
type Client struct {
	IPAddress          string
	InsecureSkipVerify bool

	disableRedirect bool
	cookie          map[string]string
	timeout         time.Duration
}

//SetTimeout ...
func (c *Client) SetTimeout(t time.Duration) *Client {
	c.timeout = t
	return c
}

//Do ...
func (c *Client) Do(req *Request) (*Response, error) {
	tr := &http.Transport{DisableKeepAlives: false, DisableCompression: false}
	if c.InsecureSkipVerify {
		tr.TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
	}

	httpClient := http.Client{Timeout: c.timeout, Transport: tr}
	httpReq, e := req.getHTTPRequest()
	if e != nil {
		return nil, e
	}
	httpResp, e := httpClient.Do(httpReq)
	if e != nil {
		return nil, e
	}
	return parseResponse(httpResp), nil
}

//DisableRedirect ...
func (c *Client) DisableRedirect() *Client {
	c.disableRedirect = true
	return c
}

//PostForm ...
func (c *Client) PostForm(req *Request) ([]byte, error) {
	form := url.Values{}
	for key, value := range req.parameterMap {
		form.Add(key, value)
	}
	httpReq, e := http.NewRequest(MethodPost, req.Header.URL, strings.NewReader(form.Encode()))
	httpReq.Header = req.Header.Header

	httpReq.Header.Add(`Content-type`, `application/x-www-form-urlencoded`)

	tr := &http.Transport{DisableKeepAlives: false, DisableCompression: false}
	if c.InsecureSkipVerify {
		tr.TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
	}
	httpClient := http.Client{Timeout: c.timeout, Transport: tr}

	if c.disableRedirect {
		httpClient.CheckRedirect = func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		}
	}
	httpResp, e := httpClient.Do(httpReq)

	if e != nil {
		logError(e)
		return nil, e
	}
	data, e := ioutil.ReadAll(httpResp.Body)
	if e != nil {
		logError(e)
		return nil, e
	}
	return data, nil
}

//NewClient ...
func NewClient() *Client {
	return new(Client)
}
