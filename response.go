/**
* Created by Visual Studio Code.
* User: tuxer
* Created At: 2018-02-05 13:57:06
**/

package net

import (
	"io/ioutil"
	"net/http"
)

//Response ...
type Response struct {
	Header *Header
	Body   []byte

	resp *http.Response
}

func parseResponse(httpResp *http.Response) *Response {
	body, _ := ioutil.ReadAll(httpResp.Body)
	resp := &Response{
		Header: parseHeader(httpResp),
		Body:   body,
		resp:   httpResp,
	}
	return resp

}
