/**
* Created by Visual Studio Code.
* User: tuxer
* Created At: 2018-04-14 21:25:05
**/

package net

import (
	"bytes"
	"net/http"
	"strings"
)

//Header ...
type Header struct {
	http.Header
	Method string
	URL    string
	Proto  string
	Host   string
}

//Bytes ...
func (h *Header) Bytes() []byte {
	buff := new(bytes.Buffer)
	h.Write(buff)
	return buff.Bytes()
}

//Set ...
func (h *Header) Set(key, value string) *Header {
	if h.Header == nil {
		h.Header = make(http.Header)
	}
	h.Header.Set(key, value)
	return h
}

//Sync ...
func (h *Header) Sync(header http.Header) {
	for key, val := range h.Header {
		header[key] = val
	}
}

//String ...
func (h *Header) String() string {
	return string(h.Bytes())
}

func parseHeader(r interface{}) *Header {
	h := new(Header)
	switch r := r.(type) {
	case *http.Request:
		h.Header = r.Header
		h.Method = r.Method
		h.URL = `http`
		if r.TLS != nil {
			h.URL += `s`
		}
		h.URL += `://` + r.Host + r.URL.Path
		if q := r.URL.Query().Encode(); q != `` {
			h.URL += `?` + q
		}
		h.Proto = r.Proto
		h.Host = r.RemoteAddr
		if i := strings.LastIndex(h.Host, `:`); i >= 0 {
			h.Host = h.Host[:i]
		}
	case *http.Response:
		h.Header = r.Header
		h.Method = r.Request.Method
		h.Proto = r.Proto
		h.Host = r.Request.Host
	}
	return h
}
