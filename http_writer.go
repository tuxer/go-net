/**
* Created by Visual Studio Code.
* User: tuxer
* Created At: 2019-10-03 13:19:44
**/

package net

import "net/http"

//HTTPWriter ...
type HTTPWriter struct {
	nativeWriter http.ResponseWriter
	buffer       []byte
	header       http.Header
	flushed      bool
}

//SetHeader ...
func (h *HTTPWriter) SetHeader(key, value string) {
	if h.header == nil {
		h.header = make(http.Header)
	}
	h.header.Set(key, value)
}

//Write ...
func (h *HTTPWriter) Write(data []byte) {
	h.buffer = append(h.buffer, data...)
}

//Flush ...
func (h *HTTPWriter) Flush() {
	if !h.flushed {
		for key := range h.header {
			h.nativeWriter.Header().Set(key, h.header.Get(key))
		}
		h.nativeWriter.Write(h.buffer)
		h.flushed = true
	}
}
