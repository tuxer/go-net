package net

import "log"

var (
	debugFunc = log.Println
	errorFunc = log.Println
	fatalFunc = log.Fatalln
	tag       = `[go-net]`
)

func logDebug(d ...interface{}) {
	if debugFunc != nil {
		debugFunc(append([]interface{}{tag}, d...)...)
	}
}
func logError(e ...interface{}) {
	if errorFunc != nil {
		errorFunc(append([]interface{}{tag}, e...)...)
	}
}
func logFatal(f ...interface{}) {
	if fatalFunc != nil {
		fatalFunc(append([]interface{}{tag}, f...)...)
	}
}

//SetLogger tag, debugFunc, errorFunc, fatalFunc
func SetLogger(t string, d, e, f func(v ...interface{})) {
	tag = `[` + t + `]`
	debugFunc = d
	errorFunc = e
	fatalFunc = f
}
