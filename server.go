package net

import (
	"crypto/tls"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"reflect"
	"strconv"
	"strings"
	"time"
)

/**
 * Created by tuxer on 11/14/17.
 */

//Server ...
type Server struct {
	httpServer *http.Server
	containers []container
	readCh     chan bool
	rootDir    string
	port       int

	certFile string
	keyFile  string

	readTimeout  time.Duration
	writeTimeout time.Duration
}

//SetReadTimeout ...
func (s *Server) SetReadTimeout(timeout time.Duration) {
	s.readTimeout = timeout
}

//SetWriteTimeout ...
func (s *Server) SetWriteTimeout(timeout time.Duration) {
	s.writeTimeout = timeout
}

//Start ...
func (s *Server) Start() {
	go func() {
		defer func() {
			if r := recover(); r != nil {
				logError(`Error Starting server`, s.port, r)
			}
		}()

		if s.certFile != `` && s.keyFile != `` {
			s.httpServer.TLSConfig = &tls.Config{InsecureSkipVerify: true}
			if e := s.httpServer.ListenAndServeTLS(s.certFile, s.keyFile); e != nil {
				logFatal(e)
			}
		} else {
			s.httpServer.ReadTimeout = s.readTimeout
			s.httpServer.WriteTimeout = s.writeTimeout
			if e := s.httpServer.ListenAndServe(); e != nil {
				logFatal(e)
			}
		}
	}()
}

//Run ...
func (s *Server) Run() error {
	ch := make(chan error)
	go func() {
		if s.certFile != `` && s.keyFile != `` {
			s.httpServer.TLSConfig = &tls.Config{InsecureSkipVerify: true}
			if e := s.httpServer.ListenAndServeTLS(s.certFile, s.keyFile); e != nil {
				ch <- e
			}
		} else {
			s.httpServer.ReadTimeout = s.readTimeout
			s.httpServer.WriteTimeout = s.writeTimeout
			if e := s.httpServer.ListenAndServe(); e != nil {
				ch <- e
			}
		}
	}()
	select {
	case <-time.After(5 * time.Second):
	case e := <-ch:
		logError(`Error starting server`, e.Error())
		return e
	}
	logDebug(`Starting server at`, s.GetPort())
	return nil
}

//GetPort ...
func (s *Server) GetPort() int {
	return s.port
}

//SetPort ...
func (s *Server) SetPort(port int) {
	s.port = port
}

//SetSecure ...
func (s *Server) SetSecure(certFile, keyFile string) {
	s.certFile = certFile
	s.keyFile = keyFile
}

//SetRootDir ...
func (s *Server) SetRootDir(rootDir string) {
	s.rootDir = rootDir
}

//AddController ...
func (s *Server) AddController(path string, controller ControllerInterface) {
	if !strings.HasPrefix(path, `/`) {
		path = `/` + path
	}
	logDebug(`Server Port`, s.port, `Add Controller:`, path)
	controllerInterface := reflect.ValueOf(controller).Elem().Interface()
	s.containers = append(s.containers, container{item: controllerInterface, path: path})
}

func (s *Server) findContainer(uri string) *container {
	if !strings.HasPrefix(uri, `/`) {
		uri = `/` + uri
	}
	for _, container := range s.containers {
		regex := container.getRegex()
		if regex != nil && regex.Match([]byte(uri)) {
			return &container
		}
	}
	if uri == `/` {
		return nil
	}
	split := strings.Split(uri, `/`)
	split = split[:len(split)-1]
	return s.findContainer(strings.Join(split, `/`))
}

func (s *Server) fetchFile(filename string) ([]byte, error) {
	body, e := ioutil.ReadFile(filename)
	if e != nil {
		return nil, e
	}
	return body, nil
}

func (s *Server) serve(w http.ResponseWriter, r *http.Request) {
	defer func() {
		if r := recover(); r != nil {
			logError(r)
		}
	}()
	u, e := url.Parse(r.RequestURI)
	if e != nil {
		panic(e)
	}

	container := s.findContainer(u.Path)
	if container == nil {
		abs, _ := filepath.Abs(s.rootDir)

		filename := abs + u.Path

		if _, e := os.Stat(filename); e == nil {
			http.ServeFile(w, r, filename)
			return
		} else if _, e := os.Stat(abs + `/index.html`); e == nil {
			filename = abs + `/index.html`
		} else if _, e := os.Stat(abs + `/index.htm`); e == nil {
			filename = abs + `/index.htm`
		} else {
			logDebug(`unable to found index file`, r.RequestURI, `from`, r.RemoteAddr)
			return
		}
		http.ServeFile(w, r, filename)
		return
	}
	controller := container.NewController(s)

	host := strings.SplitN(r.Host, `:`, 2)
	client := &Client{IPAddress: host[0]}
	controller.setClient(client)

	httpWriter := &HTTPWriter{nativeWriter: w}
	controller.ReceiveRequest(parseRequest(r), httpWriter)
	httpWriter.Flush()
}

//NewServer ...
func NewServer(port int) *Server {
	server := &Server{
		readTimeout:  60 * time.Second,
		writeTimeout: 60 * time.Second,
	}

	server.httpServer = &http.Server{
		Addr:           `:` + strconv.Itoa(port),
		Handler:        http.HandlerFunc(server.serve),
		ReadTimeout:    server.readTimeout,
		WriteTimeout:   server.writeTimeout,
		MaxHeaderBytes: 1 << 20,
	}
	server.port = port
	return server
}
