/**
* Created by Visual Studio Code.
* User: tuxer
* Created At: 2018-02-05 13:10:10
**/

package net

import (
	"bytes"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strconv"
)

//Request ...
type Request struct {
	Header          *Header
	httpRequest     *http.Request
	IPAddress       string
	parameterMap    map[string]string
	disableRedirect bool
	Body            []byte
}

//HeaderBytes ...
func (r *Request) HeaderBytes() []byte {
	b, _ := httputil.DumpRequest(r.httpRequest, false)
	return b
}

//URL ...
func (r *Request) URL() (*url.URL, error) {
	u, e := url.Parse(r.Header.URL)
	if e != nil {
		return nil, e
	}
	return u, nil
}

//Set ...
func (r *Request) Set(key, value string) *Request {
	if r.parameterMap == nil {
		r.parameterMap = make(map[string]string)
	}
	r.parameterMap[key] = value
	return r
}

//SetMethod ...
func (r *Request) SetMethod(method string) {
	r.Header.Method = method
}

//SetURL ...
func (r *Request) SetURL(url string) {
	r.Header.URL = url
}

func (r *Request) getHTTPRequest() (*http.Request, error) {
	var body io.Reader

	method := r.Header.Method
	if method == MethodPost || method == MethodPut {
		body = bytes.NewReader(r.Body)
	} else if method == MethodGet {
		u, e := r.URL()
		if e != nil {
			return nil, e
		}
		q := u.Query()
		for key, val := range r.parameterMap {
			q.Set(key, val)
		}
		u.RawQuery = q.Encode()
		r.Header.URL = u.String()
	}

	req, e := http.NewRequest(method, r.Header.URL, body)
	if e != nil {
		return nil, e
	}

	req.Header = r.Header.Header

	r.setContentLength()

	r.Header.Sync(req.Header)

	return req, nil
}

func (r *Request) setContentLength() {
	if r.Header.Method == MethodPost {
		r.Header.Set(`Content-Length`, strconv.Itoa(len(r.Body)))
	}
}

func parseRequest(httpReq *http.Request) *Request {
	req := &Request{httpRequest: httpReq}
	req.Body, _ = ioutil.ReadAll(httpReq.Body)
	req.Header = parseHeader(httpReq)
	return req
}
