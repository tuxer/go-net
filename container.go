/**
* Created by Visual Studio Code.
* User: tuxer
* Created At: 2018-04-12 14:59:12
**/

package net

import (
	"reflect"
	"strings"
	"regexp"
)

type container struct {
	path		string
	item		interface{}
	regex		*regexp.Regexp
}

func (c *container) getRegex() *regexp.Regexp {
	if c.regex == nil {
		path := `^` + c.path
		if !strings.HasSuffix(path, `.*`) {
			path += `($|/.*)`
		}
		c.regex, _ = regexp.Compile(path)
	}
	return c.regex
}

func (c *container) NewController(server *Server) ControllerInterface {
	iface := reflect.New(reflect.TypeOf(c.item)).Interface().(ControllerInterface)
	iface.setServer(server)
	return iface
}
