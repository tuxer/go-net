/**
* Created by Visual Studio Code.
* User: tuxer
* Created At: 2017-12-11 15:40:43
 */
package net

import (
	"errors"
	"net"
	"net/http"
	"time"
)

const (
	HttpOK         = http.StatusOK
	HttpBadRequest = http.StatusBadRequest
	MethodGet      = http.MethodGet
	MethodPost     = http.MethodPost
	MethodPut      = http.MethodPut
)

var (
	stdClient = NewClient()
)

//CheckTCP ...
func CheckTCP(ip string, port string) error {
	cn, e := net.DialTimeout(`tcp`, ip+`:`+port, 60*time.Second)
	if cn == nil {
		return errors.New(`Failed connecting to ` + ip + `:` + port)
	}
	return e
}

//NewRequest ...
func NewRequest(method, url string, body []byte) *Request {
	req := &Request{
		Header: &Header{
			Header: http.Header{},
			Method: method,
			URL:    url,
		},
		Body: body,
	}
	req.setContentLength()
	return req
}

//Do ...
func Do(req *Request) (*Response, error) {
	return stdClient.Do(req)
}
